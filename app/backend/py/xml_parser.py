import xmltodict, json, requests
xml_garden = [
    'https://map1.vis.earthdata.nasa.gov/wmts-webmerc/wmts.cgi?SERVICE=WorldWeather&request=GetCapabilities',
    'https://services.sentinel-hub.com/v2/wmts/56748ba2-4a88-4854-beea-86f9afc63e35?REQUEST=GetCapabilities&SERVICE=WorldWeather',
    'https://tiles.geoservice.dlr.de/service/wmts?SERVICE=WMTS&REQUEST=GetCapabilities',
    'https://oos.soest.hawaii.edu/thredds/wms/hioos/model/atm/ncep_global/NCEP_Global_Atmospheric_Model_best.ncd?service=WMS&version=1.3.0&request=GetCapabilities',
    'https://geo.weather.gc.ca/geomet/?lang=E&service=WMS&request=GetCapabilities',
    'https://wms.gsfc.nasa.gov/cgi-bin/wms.cgi?VER=1.1.1&SERVICE=WMS&REQUEST=GetCapabilities',
    'https://apps.ecmwf.int/wms/?token=public',
    'https://geoint.nrlssc.navy.mil/nrltileserver/wms?REQUEST=GetCapabilities&VERSION=1.1.1&SERVICE=WMS',
    'https://neowms.sci.gsfc.nasa.gov/wms/wms',
    'https://185.104.180.39/eumetsat?service=wms&version=1.3.0&request=GetCapabilities',
    'https://isse.cr.usgs.gov/arcgis/services/Orthoimagery/USGS_EROS_Ortho_1Foot/ImageServer/WMSServer?request=GetCapabilities&service=WMS',
    'https://isse.cr.usgs.gov/arcgis/services/Scanned_Maps/USGS_EROS_DRG_SCALE/ImageServer/WMSServer?request=GetCapabilities&service=WMS',
    'https://regclim.coas.oregonstate.edu:8080/thredds/wms/regcmdata/EH5/merged/Decadal/RegCM3_Decadal_merged_EH5.ncml?service=WMS&version=1.3.0&request=GetCapabilities',
    'https://geoservice.dlr.de/eoc/atmosphere/wms?SERVICE=WMS&REQUEST=GetCapabilities',
    'https://geoservice.dlr.de/eoc/elevation/wms?SERVICE=WMS&REQUEST=GetCapabilities', 
    'https://geoservice.dlr.de/eoc/basemap/wms?SERVICE=WMS&REQUEST=GetCapabilities',
    'https://geoservice.dlr.de/eoc/imagery/wms?SERVICE=WMS&REQUEST=GetCapabilities', 
    'https://geoservice.dlr.de/eoc/land/wms?SERVICE=WMS&REQUEST=GetCapabilities',
    'https://idpgis.ncep.noaa.gov/arcgis/services/NWS_Observations/NOHRSC_Snow_Analysis/MapServer/WMSServer?request=GetCapabilities&service=WMS',
    'https://idpgis.ncep.noaa.gov/arcgis/services/NWS_Forecasts_Guidance_Warnings/NHC_E_Pac_trop_cyclones/MapServer/WMSServer?request=GetCapabilities&service=WMS',
    'https://idpgis.ncep.noaa.gov/arcgis/services/NWS_Forecasts_Guidance_Warnings/NHC_Atl_trop_cyclones/MapServer/WMSServer?request=GetCapabilities&service=WMS',
    'https://idpgis.ncep.noaa.gov/arcgis/services/NWS_Forecasts_Guidance_Warnings/watch_warn_adv/MapServer/WMSServer?request=GetCapabilities&service=WMS',
    'https://idpgis.ncep.noaa.gov/arcgis/services/NWS_Observations/radar_base_reflectivity/MapServer/WMSServer?request=GetCapabilities&service=WMS'
    'https://mesonet.agron.iastate.edu/cgi-bin/wms/goes/conus_vis.cgi?VER=1.1.1&SERVICE=WMS&REQUEST=GetCapabilities',
    'https://mesonet.agron.iastate.edu/cgi-bin/wms/goes/conus_ir.cgi?VER=1.1.1&SERVICE=WMS&REQUEST=GetCapabilities',
    'https://mesonet.agron.iastate.edu/cgi-bin/wms/goes/conus_wv.cgi?VER=1.1.1&SERVICE=WMS&REQUEST=GetCapabilities',
    'https://mesonet.agron.iastate.edu/cgi-bin/wms/goes/east_ir.cgi?VER=1.1.1&SERVICE=WMS&REQUEST=GetCapabilities',
    'https://mesonet.agron.iastate.edu/cgi-bin/wms/goes/west_ir.cgi?VER=1.1.1&SERVICE=WMS&REQUEST=GetCapabilities',
    'https://mesonet.agron.iastate.edu/cgi-bin/wms/goes/east_vis.cgi?VER=1.1.1&SERVICE=WMS&REQUEST=GetCapabilities',
    'https://mesonet.agron.iastate.edu/cgi-bin/wms/goes/west_vis.cgi?VER=1.1.1&SERVICE=WMS&REQUEST=GetCapabilities',
    'https://mesonet.agron.iastate.edu/cgi-bin/wms/goes/east_wv.cgi?VER=1.1.1&SERVICE=WMS&REQUEST=GetCapabilities',
    'https://mesonet.agron.iastate.edu/cgi-bin/wms/goes/west_wv.cgi?VER=1.1.1&SERVICE=WMS&REQUEST=GetCapabilities'
]

def show_raw_data():
    number = 0
    for xls_source in xml_garden:
        print(f'before request - {xls_source}')
        try:
            get_xml_body = requests.get(xls_source).content
            o = xmltodict.parse(get_xml_body)
            with open(f'data{number}.json', 'w') as outfile:
                json.dump(o, outfile)

            print(f'data{number}.json are created \n')
            number += 1
        except Exception:
            print(f'this url doesnt exist')
    
    print(f'Done! There is {number} datasets \n')

if __name__ == "__main__":
    show_raw_data()
