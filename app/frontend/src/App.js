import React, { Component } from 'react';
import styles from './App.css';
import { Switch, Route } from 'react-router-dom';

// Components
// import Counter from './components/Counter';
// /Components

// Containets
import Navigation from './containers/Navigation'
//import Footer from './containers/Footer';
// /Containers


// Pages
import Home from './pages/Home';
import Event from './pages/Event';
import EventCreate from './pages/Event/create';
import BeforeAfter from './pages/BeforeAfter';
// /Pages




class App extends Component {
  render() {
    return (
    <div className={styles.App}>
        <Navigation />
        
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/event/create" component={EventCreate} />
            <Route path="/event" component={Event} />
            <Route path="/after" component={BeforeAfter} />
        </Switch>

    </div>
    );
  }
}

export default App;
