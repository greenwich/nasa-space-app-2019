// Block with infos about tour

import React, {Component} from 'react'
import postData from '../data/post.json'
// UI
/*import {
	Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button, CardLink, Col,
} from 'reactstrap'
*/
import {Grid, Card, Icon, Image, Button} from 'semantic-ui-react'






let id = 0
// TODO change name for post - it should be more informative
const post = postData.postData[id]
console.log(`id = ${id}`)
console.log(`json - ${postData.postData[id].country.name}`)




class Post extends Component {
		render() {
			return (
                            <Grid.Column >
                                  <Card>
                                    <Image src={post.country.image.url} alt={post.country.image.alt} />
                                    <Card.Content>
                                      <Card.Header>{post.country.city}, {post.country.name}</Card.Header>
                                        <Card.Subtitle itemprop="price">Цена за человека - {post.currency.dollar + post.price} на {post.nights} ночей.</Card.Subtitle>
                                      <Card.Meta>
                                        <span className='date'>Вылет из {post.flyFrom} {post.date}</span>
                                      </Card.Meta>
                                      <Card.Description>Matthew is a musician living Nashville.</Card.Description>
                                    </Card.Content>
                                    <Card.Content extra>
                                      <a>
                                        <Icon name='user' />
                                        22 одобрили
                                      </a>
                                        <div className='ui two buttons'>
                                          <Button basic color='green'href={post.link_to_full_page} alt={ post.description + post.keywords}>
                                             Хочу этот тур
                                          </Button>
                                          <Button basic color='red'>
                                            Добавить себе
                                          </Button>
                                        </div>
                                      </Card.Content>
                                  </Card>
                            </Grid.Column>
                    );
	}
}

export default Post;
