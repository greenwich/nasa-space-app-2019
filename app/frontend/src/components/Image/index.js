import React from 'react'
import { Image } from 'semantic-ui-react'

const ImageExampleFluid = () => (
  <Image src='https://images-na.ssl-images-amazon.com/images/I/81Hh6ik2fGL._SL1500_.jpg' fluid />
)

export default ImageExampleFluid
