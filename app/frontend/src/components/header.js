import React, {Component} from 'react';
import "../static/css/header.css"

class Header extends Component {
	render() {
		return (
			<header id="header" className="header">
                <p className="header__search">
                начните вводить нужные вам параметры (ценник, страну и тд)
                </p>
                <div className="header__search__form">
                    <form className="header__form__find_any__form form-inline my-2 my-lg-0">
                        <input className="header__form__find_any__form__input form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                        <button className="header__form__find_any__form__button btn btn-outline-success my-2 my-sm-0" type="submit" disabled>Сохранить результат поиска</button>
                    </form>
                </div>
            </header>
		);
	}
}


export default Header;
