import React, {Component} from 'react'
// import CSSModules from 'react-css-modules'
import PropTypes from 'prop-types'

// import Loader from 'src/components/ViewsItems/LoaderCatalog'
import LoadMore from '../LoadMore'

// import styles from './styles.scss'

// @CSSModules(styles)

class InfinityPagination extends Component {
    state ={
        visibleLoader: false,
        page: 4
    }
    scroll = (e) => {
        let windowBottonSideY = window.pageYOffset + window.innerHeight
        let paginationBottonSideY = this.pagination.offsetHeight + this.pagination.offsetTop

        if (windowBottonSideY >= paginationBottonSideY && this.state.page) {
            // this.props.nextData(this.state.page)
            console.log(this.state.page)
            this.setState(
                ({page}) => ({
                    page: --page,
                    visibleLoader: true
                }))
        }
    }

    componentDidMount () {
        window.addEventListener('scroll', this.scroll)
        console.log(this.pagination)
    }

    render () {
        return (
            <div ref={element => { this.pagination = element }}>
                {this.props.children}
                <LoadMore />
            </div>
        )
    }
}

InfinityPagination.propTypes = {
    pagesCount: PropTypes.number.isRequired,
    nextData: PropTypes.func.isRequired,
    loader: PropTypes.element,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
}

export default InfinityPagination