import React, { Component } from 'react'
import { connect } 			from 'react-redux'
import { 
	INCREMENT_COUNT,
	DEINCREMENT_COUNT,
	SET_VALUE
 } from '../../store/actions/types'
import { Button } from 'semantic-ui-react'


class Counter extends Component {
	render() {
		return (
			<div>
				<Button onClick={this.props.increment}>+</Button>
				{this.props.count}
				<Button onClick={this.props.deincrement}>-</Button>
			</div>
			)
	}
} 





export default connect(
	state => ({
		count: state.count
	}),
	dispatch => ({
		increment () {
			dispatch({type: INCREMENT_COUNT})
		},
		deincrement () {
			dispatch({type: DEINCREMENT_COUNT})
		},
		setValue(value) {
			dispatch({type: SET_VALUE, payload: value})
		}
	})
)(Counter)