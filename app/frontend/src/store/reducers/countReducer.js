import {
	INCREMENT_COUNT,
	DEINCREMENT_COUNT,
	SET_VALUE
} from '../actions/types';



export default function (state = 10, action) {
	switch(action.type) {
		case INCREMENT_COUNT: {
			return ++state
		}
		case DEINCREMENT_COUNT: {
			return --state
		}
		case SET_VALUE: {
			return --state
		}
		default: {
			return state
		}
	}
}