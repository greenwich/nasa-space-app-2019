/*
Maintainer: Pavel Liferenko <t.me/Liferenko>
Description: Container with template of post.

TODO:
  - Redux: to add id to payload
  - to add requests' parameters in URL: limit, offset, tag
  - remove a whole axios.get().then() function to ApiHandler component
  -
*/



import React, {Component} from 'react'
import { 
  Image, 
  Card }                  from 'semantic-ui-react'

// Components
// import FavoriteStar       from '../../components/FavoriteStar' 
import ReadMore       from '../../components/ReadMore'

// /Components


class PostItem extends Component {
    render() {
        
        const { event } = this.props
        console.log( {event} )
        return(
            
            <div className="post__item">
              <Card>
                <p>{ event }</p>
              </Card>
            </div>
        )

/*
        return (
            <div className="post__item">
              <Card>
                    <Image src={event.imageUrl} alt="TODO" />
                    <Card.Content>
                        <Card.Header>{event.city}, {event.name}</Card.Header>
                        <Card.Meta>
                          <span className='date'>Вылет из {event.flyFrom} {event.departureDate}</span>
                        </Card.Meta>
                        <Card.Description>Цена за человека - {event.USD + ' ' + event.price} на {event.nights} ночей.</Card.Description>
                        <Card.Description>{event.description}</Card.Description>
                      </Card.Content>
                    <Card.Content extra>
                      <Card.Meta alt={ event.description + event.keywordsKey1}>
                        <ReadMore 
                            link={event.id} />
                      </Card.Meta>
                    </Card.Content>
              </Card>
            </div>
        )
*/
    };
}



export default PostItem
