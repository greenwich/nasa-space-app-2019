import React, {Component} from 'react';

import {	  Collapse,
	          Navbar,
	          NavbarToggler,
	          NavbarBrand,
	          Nav,
	          NavItem,
	          NavLink,


} from 'reactstrap';

const contacts = {
	officePhone: '+380678306538',
	officeEmail: 'belomorska@poehalisnami.com',
	social: {
		instagram: {
                        iconUrl: '',
			url: 'https://instagram.com/anisa.poehali/',
			description: 'TODO',
		},
		facebook: {
                        iconUrl: '',
			url: 'https://facebook.com/PoehalisNamiDarynok/',
			description: 'TODO',
		},
		pinterest: {
                        iconUrl: '',

            },
	},
}

class Footer extends Component {
	constructor(props) {
		super(props);

	this.toggle = this.toggle.bind(this);
	this.state = {
		isOpen: false,
		};
	}
	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}
	
	
	render() {
		return (
		<div id="footer">
			
			<Navbar color="light" light expand="md">
			    <NavbarBrand href="/">Контакты</NavbarBrand>
                            <NavbarToggler onClick={this.toggle} />
                            <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav className="ml-auto" navbar>
				<NavItem><NavLink href={'tel:' + contacts.officePhone} target="_blank">{contacts.officePhone}</NavLink></NavItem>
                                <NavItem><NavLink href={'mailto:' + contacts.officeEmail} target="_blank">{contacts.officeEmail}</NavLink></NavItem>
                                <NavItem><NavLink href={contacts.social.instagram.url} target="_blank">Мы в Instagram</NavLink></NavItem>
                                <NavItem><NavLink href={contacts.social.facebook.url} target="_blank">Мы в Facebook</NavLink></NavItem>
                                
                            </Nav>
                            </Collapse>
			</Navbar>
		</div>
		);
	}
}

export default Footer;
