import React from 'react'
import { Button, Card, Image } from 'semantic-ui-react'

const CardGroups = () => (
  <Card.Group>
    <Card>
      <Card.Content>
        <Image
          floated='right'
          size='mini'
          src='https://react.semantic-ui.com/images/avatar/large/steve.jpg'
        />
        <Card.Header>Clean beach & Play Volleyball</Card.Header>
        <Card.Meta>Gagik Chovelidze</Card.Meta>
        <Card.Description>
          Gagik needs 7 more people <strong>on Jun.29</strong>
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <div className='ui two buttons'>
          <Button basic color='green'>
            Join
          </Button>
          <Button basic color='red'>
            Decline
          </Button>
        </div>
      </Card.Content>
    </Card>
    <Card>
      <Card.Content>
        <Image
          floated='right'
          size='mini'
          src='https://react.semantic-ui.com/images/avatar/large/molly.png'
        />
        <Card.Header>Wildfire preparation Group</Card.Header>
        <Card.Meta>Mariam Kozak</Card.Meta>
        <Card.Description>
          Mariam orginize a emergency group for village preparation <strong>on Jul.8</strong>
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <div className='ui two buttons'>
          <Button basic color='green'>
            Join
          </Button>
          <Button basic color='red'>
            Decline
          </Button>
        </div>
      </Card.Content>
    </Card>
    <Card>
      <Card.Content>
        <Image
          floated='right'
          size='mini'
          src='https://react.semantic-ui.com/images/avatar/large/jenny.jpg'
        />
        <Card.Header>Free group "Zero waste lifestyle"</Card.Header>
        <Card.Meta>Pablo Pasaporte</Card.Meta>
        <Card.Description>
          How to clean your future, Jul.9-10
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <div className='ui two buttons'>
          <Button basic color='green'>
            Join
          </Button>
          <Button basic color='red'>
            Decline
          </Button>
        </div>
      </Card.Content>
    </Card>
    <Card>
      <Card.Content>
        <Image
          floated='right'
          size='mini'
          src='https://react.semantic-ui.com/images/avatar/large/jenny.jpg'
        />
        <Card.Header>Free group "Zero waste lifestyle"</Card.Header>
        <Card.Meta>Pablo Pasaporte</Card.Meta>
        <Card.Description>
          How to clean your future, Jul.9-10
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <div className='ui two buttons'>
          <Button basic color='green'>
            Join
          </Button>
          <Button basic color='red'>
            Decline
          </Button>
        </div>
      </Card.Content>
    </Card>
    <Card>
      <Card.Content>
        <Image
          floated='right'
          size='mini'
          src='https://react.semantic-ui.com/images/avatar/large/molly.png'
        />
        <Card.Header>Wildfire preparation Group</Card.Header>
        <Card.Meta>Mariam Kozak</Card.Meta>
        <Card.Description>
          Mariam orginize a emergency group for village preparation <strong>on Jul.8</strong>
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <div className='ui two buttons'>
          <Button basic color='green'>
            Join
          </Button>
          <Button basic color='red'>
            Decline
          </Button>
        </div>
      </Card.Content>
    </Card>
    <Card>
      <Card.Content>
        <Image
          floated='right'
          size='mini'
          src='https://react.semantic-ui.com/images/avatar/large/jenny.jpg'
        />
        <Card.Header>Free group "Zero waste lifestyle"</Card.Header>
        <Card.Meta>Pablo Pasaporte</Card.Meta>
        <Card.Description>
          How to clean your future, Jul.9-10
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <div className='ui two buttons'>
          <Button basic color='green'>
            Join
          </Button>
          <Button basic color='red'>
            Decline
          </Button>
        </div>
      </Card.Content>
    </Card>
  </Card.Group>
)

export default CardGroups;
