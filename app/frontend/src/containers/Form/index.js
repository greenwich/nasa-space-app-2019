import React from 'react';
import { Form, Input, TextArea, Button, Select } from 'semantic-ui-react';

const cityOptions = [
  { key: 'm', text: 'Nazuki', value: 'Nazuki' },
  { key: 'f', text: 'Paris', value: 'Paris' },
  { key: 'o', text: 'Tbilisi', value: 'Tbilisi' },
]

const FormExampleFieldControlId = () => (
  <Form>
    <Form.Group widths='equal'>
      <Form.Field
        id='form-input-control-first-name'
        control={Input}
        label='Event name'
        placeholder='Event name'
      />
      <Form.Field
        control={Select}
        options={cityOptions}
        label={{ children: 'Location', htmlFor: 'form-select-control-gender' }}
        placeholder='Location'
        search
        searchInput={{ id: 'form-select-control-gender' }}
      />
    </Form.Group>
    <Form.Field
      id='form-textarea-control-opinion'
      control={TextArea}
      label='Description'
      placeholder='Description'
    />
    <Form.Field
      id='form-button-control-public'
      control={Button}
      content='Create your event'
      label='Create your event'
    />
  </Form>
)

export default FormExampleFieldControlId;
