import React, {Component} from 'react';
import {
	  Collapse,
	  Navbar,
	  NavbarToggler,
	  NavbarBrand,
	  Nav,
	  NavItem,
	  NavLink,
          UncontrolledDropdown,
	  DropdownToggle,
	  DropdownMenu,
	  DropdownItem,
} from 'reactstrap';
import { Link } from 'react-router-dom';






class Navigation extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }


  render() {
    return (
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/">We.are.Earthlings</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink><Link to="/">Map</Link></NavLink>
              </NavItem>
              <NavItem>
                <NavLink><Link to="/after">Signs</Link></NavLink>
              </NavItem>	
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Events
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    <Link to="/event/create">Create event</Link>
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    
                    <Link to="/event/join">Join event</Link>
                  </DropdownItem>
                  <DropdownItem divider />
                </DropdownMenu>
              </UncontrolledDropdown>
            


            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

export default Navigation;
