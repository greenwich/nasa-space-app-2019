import React, {Component} from 'react'

// Components
//import WorldWind from '../../static/worldwind.min'
// /Components

class Map extends Component {
    componentDidMount() {
        this.updateCanvas();
    }

    updateCanvas() {
        const ctx = this.refs.canvas.getContext('2d');
        ctx.fillRect(0,0, 100, 100);
        
                let globe = function eventWindowLoaded() {
                    // Create a WorldWindow for the canvas.
                    var wwd = new WorldWind.WorldWindow("canvasOne");

                    // Add some image layers to the WorldWindow's globe.
                    wwd.addLayer(new WorldWind.BMNGOneImageLayer());
                    wwd.addLayer(new WorldWind.BingAerialWithLabelsLayer());

                    // Add a compass, a coordinates display and some view controls to the WorldWindow.
                    wwd.addLayer(new WorldWind.CompassLayer());
                    wwd.addLayer(new WorldWind.CoordinatesDisplayLayer(wwd));
                    wwd.addLayer(new WorldWind.ViewControlsLayer(wwd));
                }
    }

    render() {
        
        const { tour } = this.props

        return (
            <canvas ref="canvas" width={300} height={300}/>

         /*   <canvas id="canvasOne" width="1024" height="768" /> */
        )
    };
}



export default Map;

