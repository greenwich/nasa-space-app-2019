/*
Maintainer: Pavel Liferenko <t.me/Liferenko>
Description: Feed with infinite scroll pagination and post items displaying. 

TODO:
  - to add ability to use this Feed for different types of cards: posts, employees, filtered posts, comments, etc.
*/

import React, { Component } from 'react'
import axios from 'axios'

// Components
// import { APIURL } from '../../components/ApiHandler'
import Data0 from '../../data/json/data0'

// Containers
import PostItem from '../PostItem'




class Feed extends Component {
    constructor(props) {
        super(props)
        this.state = {
            apiUrl: Data0,
            jsonFromApi: [],
            
        }
    }

    componentDidMount() {
          const jsonFromApi = JSON.stringify(this.state.apiUrl)  
    } 

    render() {
        return (
            <div className="row feed">
                    <div className="ui four doubling stackable cards feed_module">
                    {
                          <PostItem 
                            event={this.state.jsonFromApi}
                            />
                    }
                    </div>
            </div>
        );
    }
}


export default Feed
