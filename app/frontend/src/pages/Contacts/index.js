import React, { Component } from 'react'
import axios from 'axios'
import { Button, Card, Image } from 'semantic-ui-react'
import { APIURL } from '../../components/ApiHandler'

console.log(`Contacts - ${APIURL}`)

class Contacts extends Component {
    constructor(props) {
        super(props)
        this.state = {
            apiUrl: `${APIURL}/employees`,
            postFromApi: [],
        }
    }

    componentDidMount() {
        axios.get(this.state.apiUrl, { crossdomain: true })
             .then(response => {
                const postFromApi = response.data;
                console.log(postFromApi);
                this.setState({ postFromApi });
                })
             .catch(error => {
                console.log(`Can't find any data in this URL - ${this.state.apiUrl}`);
                const postFromApi = [
                    {   
                        id          : 0,
                        firstname   : 'Аниса',
                        secondname  : 'Сафи',
                        catchPhrase : 'Ваш лучший отдых - наша лучшая работа',
                        city        : 'Киев',
                        avatarUrl   : 'http://placekitten.com/100/100',
                        likes       : 0,
                    },
                    {   
                        id          : 1,
                        firstname        : 'Юля',
                        secondname     : 'Чупрун',
                        catchPhrase : 'Путешествие начинается с чашки кофе',
                        city        : 'Киев',
                        avatarUrl   : 'http://placekitten.com/100/100',
                        likes       : 0,
                    },
                    {   
                        id          : 0,
                        firstname        : 'Наташа',
                        secondname     : 'Фамилия',
                        catchPhrase : 'Умеем. Можем. Просто обожаем! :)',
                        city        : 'Киев',
                        avatarUrl   : 'http://placekitten.com/100/100',
                        likes       : 0,
                    },
                ];
                this.setState({ postFromApi })
                })
    }



    render() {
        return (
          <Card.Group>
            { 
                this.state.postFromApi.map(person => (
                    <Card>
                      <Card.Content>
                        <Image floated='right' size='mini' src={person.avatarUrl} />
                        <Card.Header>{person.firstname} {person.secondname}</Card.Header>
                        <Card.Meta>{person.city}</Card.Meta>
                        <Card.Description>
                          {person.catchPhrase}
                        </Card.Description>
                      </Card.Content>
                      <Card.Content extra>
                        <div className='ui two buttons'>
                          <Button basic color='green'>
                            Позвонить
                          </Button>
                          <Button basic color='red'>
                            Написать email
                          </Button>
                        </div>
                      </Card.Content>
                    </Card>
                    )
                )
            }
          </Card.Group>
        );
    }
}

export default Contacts

