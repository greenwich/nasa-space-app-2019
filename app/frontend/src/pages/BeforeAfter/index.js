import React, { Component } from 'react';

import { Container, Header } from 'semantic-ui-react';
// import components
import BeforeAfterSlider from 'react-before-after-slider'
// /import components

import before from '../../data/beforeafter/2_Aral_Sea_B.jpeg';
import after from '../../data/beforeafter/1_1_Aral_Sea_A.jpg';

class BeforeAfter extends Component {
  render() {

       return (
           <Container>
            <div className='beforeafter'>
               <Header as='h2'>Aral sea (2000-2015)</Header>
                 <BeforeAfterSlider
                    before={before}
                    after={after}
                    width={318}
                    height={480}
                  />
            </div>

            <div className='beforeafter'>
               <Header as='h2'>Flooding in Kerala, India (Feb.2018-Aug.2018)</Header>
                 <BeforeAfterSlider
                    after='https://climate.nasa.gov/system/gallery_images/mobile/1_heilprin_and_tracy_glacierskerala_oli_201837_2048x1536-before.'
                    before='https://climate.nasa.gov/system/gallery_images/mobile/2_heilprin_and_tracy_glacierskerala_msi_2018234_2048x1536-after.'
                    width={318}
                    height={480}
                  />
               </div>

            <div className='beforeafter'>
               <Header as='h2'>Human activity threatens Nigeria’s Okomu Forest (1984-2017)</Header>
                 <BeforeAfterSlider
                    after='https://climate.nasa.gov/system/gallery_images/mobile/1_Okomu_Forest_12111984-2048px-90-before.jpg'
                    before='https://climate.nasa.gov/system/gallery_images/mobile/2_Okomu_Forest_01042017-2048px-90-after.jpg'
                    width={318}
                    height={480}
                  />
               </div>

            <div className='beforeafter'>
               <Header as='h2'>Antarctica’s Brunt Ice Shelf prepares to calve iceberg (1986-2019)</Header>
                 <BeforeAfterSlider
                    after='https://climate.nasa.gov/system/gallery_images/mobile/1_brunt_tm5_1986030_2048x1536-before.jpg'
                    before='https://climate.nasa.gov/system/gallery_images/mobile/2_brunt_oli_2019023_2048x1536-after.jpg'
                    width={318}
                    height={480}
                  />
               </div>
        </Container>
        )
    }
}

export default BeforeAfter;
