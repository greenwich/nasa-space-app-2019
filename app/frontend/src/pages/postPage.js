import React, { Component } from 'react';

// import components
import Nav from '../containers/nav';
import Header from '../containers/header';
import Search from '../containers/search';
import Footer from '../containers/footer';
// /import components


class PostPage extends Component {
    render() {
        return ([
        <Nav />,
        <Header />,
        <Search />,
        <Footer />
        ]


        )
    }
}

export default PostPage;
