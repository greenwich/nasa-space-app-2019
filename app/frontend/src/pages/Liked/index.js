import React, { Component } from 'react';


// import components
import SearchBlock from '../../containers/Search';
import Feed from '../../containers/Feed';
// /import components


class Liked extends Component {
  render() {
       return (
        <div className="column grid">
            <a class="ui teal tag label"><h1>Вам понравились эти туры</h1></a>
            <SearchBlock />
            <Feed />
        </div>
        )
    }
}

export default Liked;
