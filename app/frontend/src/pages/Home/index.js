import React, { Component } from 'react';

// import components
import Data0 from '../../data/json/data0.json';
import Data1 from '../../data/json/data4.json';
import Data2 from '../../data/json/data2.json';
import Data3 from '../../data/json/data3.json';
// import SearchBlock from '../../containers/Search';
import Globe from 'worldwind-react-globe';
import './Home.css'

// /import components


class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            apiUrl0: Data0,
            jsonFromApi0: [],
            
            apiUrl1: Data1,
            jsonFromApi1: [],

            apiUrl2: Data2,
            jsonFromApi2: [],

            apiUrl3: Data3,
            jsonFromApi3: [],
        }
    }
/*
    componentDidMount() {
            const jsonFromApi0 = JSON.stringify(this.state.apiUrl0)
          this.setState( { jsonFromApi0 } )   
            const jsonFromApi1 = JSON.stringify(this.state.apiUrl1)
          this.setState( { jsonFromApi1 } )   
            const jsonFromApi2 = JSON.stringify(this.state.apiUrl2)
          this.setState( { jsonFromApi2 } )   
            const jsonFromApi3 = JSON.stringify(this.state.apiUrl3)
          this.setState( { jsonFromApi3 } )   
    } 
*/
  render() {
      const layers = [
          //'eox-sentinal2-labels',
          'usgs-imagery-topo',
          'renderables',
          'coordinates',
          'stars',
          'atmosphere-day-night',
          //'bing-aerial-labels'
      ];

        console.log(this.state.jsonFromApi)

       return ([
           <div>
               <div className='fullscreen'>
                   <Globe 
                        ref={this.globeRef}
                        layers={layers}
                        latitude={41.71}
                        longitude={44.78}
                        altitude={10e6} 
                   />
               </div>
           </div>
        ]
        )
    }
}

export default Home;
