import React, { Component } from 'react';

// import components
import FormExampleFieldControlId from '../../../containers/Form';
// /import components
import { Container, Header } from 'semantic-ui-react'


class EventCreate extends Component {
  render() {
       return (
          <Container text>
            <Header as='h2'>Create an event</Header>
            <p>
                Launch your own event and invite people to join you. It would be your part of future.
            </p>
            <FormExampleFieldControlId className='form' />
          </Container>
        )
    }
}

export default EventCreate;
