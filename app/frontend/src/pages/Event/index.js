import React, { Component } from 'react';
import CardGroups from '../../containers/CardGroups';
import { Container, Header } from 'semantic-ui-react'

class Event extends Component {
  render() {
       return ([
          <Container text>
            <Header as='h2'>All events</Header>
            <p>
                Below are all invitations for events. People are waiting for you to joing. Planet is waiting for your action.    
            </p>

           <CardGroups />
          </Container>
        ]
        )
    }
}

export default Event;
