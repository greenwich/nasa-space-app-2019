import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
//SemanticUI
import 'semantic-ui-css/semantic.min.css';
import { BrowserRouter } from 'react-router-dom';
import './index.css';
import App from './App';
// import component
import './static/css/main.css';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import configureStore from './store';



ReactDOM.render(	
				<Provider store={configureStore()}>
					<BrowserRouter>
                    	<App />
                	</BrowserRouter>
                </Provider>, 
                document.getElementById('root'));
registerServiceWorker();
